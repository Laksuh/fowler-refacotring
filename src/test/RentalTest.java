package test;

import main.Movie;
import main.PriceCode;
import main.Rental;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;


public class RentalTest {

    private Rental rental;
    private Movie movie;
    private final int daysRented = 3;

    @BeforeEach
    void setUp() {
        movie = new Movie("Movietitle", PriceCode.NEW_RELEASE);
        rental = new Rental(movie, daysRented);
    }

    @Test
    void getDaysRented() {
        assertEquals(daysRented, rental.getDaysRented());
    }

    @Test
    void getMovie() {
        assertEquals(movie, rental.getMovie());
    }

    @Test
    void getAmountMovie1() {
        Movie movie1 = new Movie("movie1", PriceCode.NEW_RELEASE);
        Rental rental1 = new Rental(movie1, 3);
        assertEquals(9, rental1.getAmount());
    }

    @Test
    void getAmountMovie2() {
        Movie movie2 = new Movie("movie2", PriceCode.CHILDRENS);
        Rental rental2 = new Rental(movie2, 3);
        assertEquals(1.5, rental2.getAmount());
    }

    @Test
    void getAmountMovie3() {
        Movie movie3 = new Movie("movie3", PriceCode.REGULAR);
        Rental rental3 = new Rental(movie3, 3);
        assertEquals(3.5, rental3.getAmount());
    }
}