package test;

import main.Customer;
import main.Movie;
import main.PriceCode;
import main.Rental;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class CustomerTest {

    private Customer customer;

    @BeforeEach
    void setUp() {
        customer = new Customer("TestName");
    }

    @Test
    void addRental() {
        Rental rentalToAdd = new Rental(new Movie("TestMovie", PriceCode.CHILDRENS), 3);
        customer.addRental(rentalToAdd);
        assertFalse(customer.getRentals().isEmpty());
    }

    @Test
    void statement() {
        assertTrue(customer.statement().contains("for TestName"));

        // Test if amount is correct
        customer.addRental(new Rental(new Movie("testmovie 1", PriceCode.CHILDRENS), 3));
        customer.addRental(new Rental(new Movie("testmovie 2", PriceCode.REGULAR), 2));
        customer.addRental(new Rental(new Movie("testmovie 3", PriceCode.NEW_RELEASE), 2));

        assertTrue(customer.statement().contains("\t" + "\t" + 3 + "\t" + 1.5 + "\n"));
        assertTrue(customer.statement().contains("\t" + "\t" + 2 + "\t" + 2.0 + "\n"));
        assertTrue(customer.statement().contains("\t" + "\t" + 2 + "\t" + 6.0 + "\n"));

        // Test if total amount is correct
        assertTrue(customer.statement().contains("Amount owed is " + 9.5));

        // Test if frequent renter points are correct
        assertTrue(customer.statement().contains("earned 4 frequent"));
    }

}
