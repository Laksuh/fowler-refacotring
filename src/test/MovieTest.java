package test;

import main.Movie;
import main.PriceCode;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class MovieTest {

    private Movie movie;
    private PriceCode priceCode;

    @BeforeEach
    void setUp() {
        priceCode = PriceCode.NEW_RELEASE;
        movie = new Movie("TestMovie", priceCode);
    }

    @Test
    void getPriceCode() {
        assertEquals(priceCode, movie.getPriceCode());
    }

    @Test
    void getTitle() {
        assertEquals("TestMovie", movie.getTitle());
    }

    @Test
    void setPriceCode() {
        movie.setPriceCode(priceCode);
        assertEquals(priceCode, movie.getPriceCode());
    }


}
