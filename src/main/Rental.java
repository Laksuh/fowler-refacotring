package main;

public class Rental {

    private final Movie movie;
    private final int daysRented;

    public Rental(Movie newmovie, int newdaysRented) {
        movie = newmovie;
        daysRented = newdaysRented;
    }

    public int getDaysRented() {
        return daysRented;
    }

    public Movie getMovie() {
        return movie;
    }

    public double getAmount() {
        double thisAmount = 0;
        PriceCode pc = this.getMovie().getPriceCode();
        thisAmount += pc.ammount;
        if (this.getDaysRented() > pc.daysrent) {
            thisAmount += (this.getDaysRented() - pc.daysrent) * pc.factor;
        }
        return thisAmount;

    }


}