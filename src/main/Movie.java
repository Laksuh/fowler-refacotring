package main;

public class Movie {

    private final String title;
    private PriceCode priceCode;

    public Movie(String newtitle, PriceCode newpriceCode) {
        title = newtitle;
        priceCode = newpriceCode;
    }

    public PriceCode getPriceCode() {
        return priceCode;
    }

    public String getTitle (){
        return title;
    }

    public void setPriceCode(PriceCode pc) {
        priceCode = pc;
    }

}