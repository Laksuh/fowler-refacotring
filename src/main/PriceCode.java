package main;

public enum PriceCode {

    CHILDRENS(2, 1.5,1.5,3),
    REGULAR(0,2,1.5,2),
    NEW_RELEASE(1, 0,3.0,0);

    int priceCode;
    double ammount;
    double factor;
    int daysrent;

    PriceCode(int pricecode, double ammount, double factor, int daysrent){
        this.priceCode=pricecode;
        this.ammount=ammount;
        this.factor=factor;
        this.daysrent=daysrent;
    }

}
